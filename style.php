<?php
// Using PHP so that we can define all the colors ahead of time without having to search through the style sheet.


header('Content-type: text/css');


//Define a palette of five colors here

$palette=array('#000', // Primary Text Color
				'#FFF', // Title Color
				'',  //Extra
				'#C7F1C3', // Links Color
				'#C7F1C3', // Visited Links Colors
				'#FFF', // non primary text color
				'#6B5F4A', // Page Background
				'#918264', // Node Background
				'#4A5D57', // Block Background
				'#4A5D57', // Box Background
				'#A1987B');// Nav buttons background

echo '

/*
** HTML elements
*/


ul.primary li.active a {
	background-color: '.$palette[7].';
	-moz-border-radius-topleft: 20px;
	-moz-border-radius-topright: 20px;
}

tr.odd td a,tr.even	td a {
	color: #000;
}
	
ul.primary li a {
	background-color: '.$palette[10].';
	-moz-border-radius-topleft: 20px;
	-moz-border-radius-topright: 20px;
}

body {
 color: '.$palette[0].';
  background-color: '.$palette[6].';
  margin: 0;
  padding: 0;
  font-family: georgia, serif;
  font-size: small;
}
tr.odd td, tr.even td {
  padding: 0.3em;
}
h1, h2, h3, h4, h5, h6 {
  margin-bottom: 0.5em;
}
h1 {
  font-size: 1.3em;
}
h2 {
  font-size: 1.2em;
}
h3, h4, h5, h6 {
  font-size: 1.1em;
}
p {
  margin-top: 0.5em;
  margin-bottom: 0.9em;
}
a {
  text-decoration: none;
  
}
a:link {
  color: '.$palette[3].';
}
a:visited {
  color: '.$palette[4].';
}
a:hover {
  color: '.$palette[3].';
  text-decoration: underline;
}
fieldset {
  border: 1px solid #ccc;
}
pre {
  background-color: '.$palette[5].';
  padding: 0.75em 1.5em;
  font-size: 12px;
  border: 1px solid #ddd;
}
table {
  /* make <td> sizes relative to body size! */
  font-size: 1em;
}
.form-item label {
  font-size: 1em;
  color: '.$palette[0].';
}
.item-list .title {
  font-size: 1em;
  color: '.$palette[1].';
}
.links {
  margin-bottom: 0em;
}
.comment .links {
  margin-bottom: 0em;
}

/*
** Page layout blocks / IDs
*/
#header, #content {
  width: 100%;
  padding: 10px;
}

#logo {
  vertical-align: top;
  border: 0;
  width:450px;
}
img {
  float: left;
  padding: 0em 1.0em 0em 1em;
  border: 0;
}
#menu {
  padding: 0.5em 0.5em 0 0.5em;
  text-align: right;
  vertical-align: middle;
}

.menu {
 font-family:arial,sans-serif;
}


ul#nav,ul#nav li{list-style-type:none;margin:0;padding:0}
ul#nav{float:right;font-size: 80%}
ul#nav li{float:left;margin-left: 3px;text-align: center}
ul#nav a{float:left;width: 85px;padding: 5px 0;background: '.$palette[10].';text-decoration:none;color: '.$palette[3].'; margin-bottom: 5px}
ul#nav a:hover{background: '.$palette[10].';color: '.$palette[4].'}
ul#nav li.activelink a,ul#nav li.activelink a:hover{background: #FFF;color: #003}

#primary {
  font-size: 1.0em;
  padding: 0em 0.8em 0.5em 0;
  color: #9cf;
  background:#ddd;
}

#primary a, #nav li a {
  font-weight: bold;
  color: #000;
  font-size: 1.2em;
}
#secondary {
  padding: 0 1em 0.5em 0;
  font-size: 0.8em;
  color: #9cf;
  bacground: #ddd;
}
#secondary a {

  color: #9cf;
}
#search .form-text, #search .form-submit {
  border: 1px solid #369;
  font-size: 1.1em;
  height: 1.5em;
  vertical-align: middle;
}
#search .form-text {
  width: 8em;
  padding: 0 0.5em 0 0.5em;
}
#search {
	text-align:right;
	position: relative;
	right: 14px;
}
#mission {
  padding: 1.5em 2em;
  color: '.$palette[0].';
}
#mission a, #mission a:visited {
  color: '.$palette[3].';

}
.site-name {
  margin: 0.6em 0em 0em 0em;
  padding: 0em;
  font-size: 2em;
}
.site-name a:link, .site-name a:visited {
  color: '.$palette[1].';
}
.site-name a:hover {
  color: '.$palette[1].';
  text-decoration: none;
}
.site-slogan {
  font-size: 1em;
  color: '.$palette[5].';
  display: block;
  margin: 0em 0em 0em 0em;
  font-style: italic;

}
#main {
  /* padding in px not ex because IE messes up 100% width tables otherwise */
  padding: 10px;
}
#mission, .node .content, .comment .content {
  line-height: 1.4;
}
#help {
  font-size: 0.9em;
  margin-bottom: 1em;
}
.breadcrumb {
  margin: .5em;
}
.messages {
  background-color: '.$palette[9].';
  border: 1px solid #ccc;
  padding: 0.3em;
  margin-bottom: 1em;
}
.error {
  border-color: red;
}
#sidebar-left, #sidebar-right {
  
  width: 16em;
  /* padding in px not ex because IE messes up 100% width tables otherwise */
  padding: 2	px;
  vertical-align: top;
}
#footer {
  background-color: '.$palette[8].';
  padding: 10px;
  font-size: 1em;
  text-align: center;
}

/*
** Common declarations for child classes of node, comment, block, box, etc.
** If you want any of them styled differently for a specific parent, add
** additional rules /with only the differing properties!/ to .parent .class.
** See .comment .title for an example.
*/
.title, .title a {
  font-weight: bold;
  font-size: 1.3em;
  color: '.$palette[1].';
  margin: 0 auto 0 auto;  /* decrease default margins for h<x>.title */
}
.submitted {
  color: '.$palette[4].';
  font-size: 0.8em;
}
.links {
  color: '.$palette[3].';
}
.links a {

}

.box {
  padding: 0 0 1.5em 0;
}

.block {
	background: '.$palette[8].';
	margin: 1em;
	padding: 10px;
}

.title {
  margin-bottom: .25em;
}
.box .title {
  font-size: 1.1em;
}
.node {
  padding: 10px;
  background: '.$palette[7].';
  margin-bottom: 1em;
  margin-left: 0em;
}
.sticky {
  padding: 5px;
  background-color: '.$palette[7].';
  border: solid 1px #ddd;
}
.content, .comment .content {
  margin: .5em 0 .5em 0;
}

.node .taxonomy {
  color: '.$palette[0].';
  font-size: 0.8em;
  padding: 20px;
}
.picture {
  border: 1px solid #ddd;
  float: right;
  margin: 0.5em;
}
.comment {
  border: 0px solid #abc;
  padding: .5em;
  margin-bottom: 1em;
}
.comment .title a {
  font-size: 1.1em;
  font-weight: normal;
}
.comment .new {
  text-align: right;

  font-size: 0.8em;
  float: right;
  color: red;
}
.comment .picture {
  border: 0px solid #abc;
  float: right;
  margin: 0.5em;
}

/*
** Module specific styles
*/
#aggregator .feed-source {
  background-color: #eee;
  border: 1px solid #ccc;
  padding: 1em;
  margin: 1em 0 1em 0;
}
#aggregator .news-item .categories, #aggregator .source, #aggregator .age {
  color: #999;
  font-style: italic;
  font-size: 0.9em;
}
#aggregator .title {
  margin-bottom: 0.5em;
  font-size: 1em;
}
#aggregator h3 {
  margin-top: 1em;
}
#forum table {
  width: 100%;
}
#forum td {
  padding: 0.5em 0.5em 0.5em 0.5em;
}
#forum td.forum, #forum td.posts {
  background-color: #eee;
}
#forum td.topics, #forum td.last-reply {
  background-color: #ddd;
}
#forum td.container {
  background-color: #ccc;
}
#forum td.container a {
  color: #555;
}
#forum td.statistics, #forum td.settings, #forum td.pager {
  height: 1.5em;
  border: 1px solid #bbb;
}
#forum td .name {
  color: #96c;
}
#forum td .links {
  padding-top: 0.7em;
  font-size: 0.9em;
}
#profile .profile {
  clear: both;
  border: 1px solid #abc;
  padding: .5em;
  margin: 1em 0em 1em 0em;
}
#profile .profile .name {
  padding-bottom: 0.5em;
}
.block-forum h3 {
  margin-bottom: .5em;
}
.calendar a {
  text-decoration: none;
}
.calendar td, .calendar th {
  padding: 0.4em 0;
  border-color: #888;
}
.calendar .day-today {
  background-color: #69c;
}
.calendar .day-today a {
  color: #fff;
}
.calendar .day-selected {
  background-color: #369;
  color: #fff;
}
.calendar .header-week {
  background-color: #ccc;
}
.calendar .day-blank {
  background-color: #ccc;
}
.calendar .row-week td a:hover {
  background-color: #fff; color: #000;
}';
?>