Readme
------

This theme, for Drupal 4.7, utilized the Nifty Corners Cube javascript library (http://www.html.it/articoli/niftycube/index.html) based on the Drupal bluemarine theme. 

Colors can be easily altered by editing the variables commented in the first few lines of style.php.

Any comments can be sent to: drew@intramedia.net

Author
------

Andrew Lee <drew@intramedia.net>