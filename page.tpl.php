<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="<?php print $language ?>" xml:lang="<?php print $language ?>">
<head>
  <title><?php print $head_title ?></title>
  <?php
  	print $head;
    print theme('stylesheet_import', base_path() . path_to_theme() . '/style.php', 'all'); 
	print $styles;
  ?>
  <script type="text/javascript"><?php /* Needed to avoid Flash of Unstyle Content in IE */ ?> </script>
  <link rel="stylesheet" type="text/css" href="<?php print $base_path.$directory ?>/NiftyCube/niftyCorners.css">
  <script type="text/javascript">
	if (isJsEnabled()) {
	addLoadEvent(function(){
	Nifty("div.comment,div#footer,div.node","big");
	Nifty("div#menu a","medium");
	Nifty("div.block","tl bottom big");
	})
	}
  </script>

</head>
<body>
<table border="0" cellpadding="0" cellspacing="0" id="header">
  <tr>
    <td id="logo">
      <?php if ($logo) { ?><a href="<?php print $base_path ?>" title="<?php print t('Home') ?>"><img src="<?php print $logo ?>" alt="<?php print t('Home') ?>" /></a><?php } ?>	
      <?php if ($site_name) { ?><h1 class='site-name'><a href="<?php print $base_path ?>" title="<?php print t('Home') ?>"><?php print $site_name ?></a></h1><?php } ?>
      <?php if ($site_slogan) { ?><div class='site-slogan'><?php print $site_slogan ?></div><?php } ?>
    </td>
	<td class="search">
      <?php print $search_box ?>
    </td>
	</tr><tr><td id="breadcrumb"><?php print $breadcrumb ?></td><td id="menu">
          <?php if ($primary_links) : ?>
     <ul id="nav">
      <?php foreach ($primary_links as $link): ?>
       <li><?php print $link; ?></li>
      <?php endforeach; ?>
     </ul>
    <?php elseif ($secondary_links) : ?>
     <ul id="nav">
      <?php foreach ($secondary_links as $link): ?>
       <li><?php print $link; ?></li>
      <?php endforeach; ?>
     </ul>
    <?php endif; ?></td>
  </tr>
  <tr>
    <td colspan="2"><div><?php print $header ?></div></td>
  </tr>
</table>
<table border="0" cellpadding="0" cellspacing="0" id="content">
  <tr>
    <?php if ($sidebar_left) { ?><td id="sidebar-left">
      <?php print $sidebar_left ?>
    </td><?php } ?>
    <td valign="top">
      <?php if ($mission) { ?><div id="mission"><?php print $mission ?></div><?php } ?>
      <div id="main">
        
        <h1 class="title"><?php print $title ?></h1>
        <div class="tabs"><?php print $tabs ?></div>
        <?php print $help ?>
        <?php print $messages ?>
        <?php print $content; ?>
      </div>
    </td>
    <?php if ($sidebar_right) { ?><td id="sidebar-right">
      <?php print $sidebar_right ?>
    </td><?php } ?>
  </tr>
</table>
<?php if ($footer_message) { ?>
<div id="footer">
  <?php print $footer_message ?>
</div>
<?php } ?>

<?php print $closure;?>
</body>
</html>
